---
hide:
  - footer
---

# Introduktion

Fundamentet for eksamen i fagene **Netværks- og kommunikationssikkerhed** samt **Systemsikkerhed**, tager begge udgangspunkt i dette projekt (herefter omtalt som semesterprojektet).  

Projektet udarbejdes af en gruppe studerende som en praktisk implementation af hhv. **Netværks- og kommunikationssikkerhed** samt **Systemsikkerhed** i et virtuelt miljø.  

Nedenstående afsnit beskriver formål og krav til semesterprojektet. Afsnittene udgør samlet projektbeskrivelsen.

## Fag

Semesterprojektet skal inddrage centrale elementer fra fagene **Netværks- og kommunikationssikkerhed** samt **Systemsikkerhed** med henblik på at arbejde praktisk med begge fag. Fagene indeholder sammenlignelige strategier og principper, såsom "Defense in Depth" og "overvågning". Det vil sige, at der i projektet skal udarbejdes en holistisk løsning med inddragelse af samspil mellem emner og læringsmål fra begge fag.

# Læringsmål

Læringsmålene er beskrevet i den [nationale studieordning](https://esdhweb.ucl.dk/D22-1980440.pdf).

## Netværks- og kommunikationssikkerhed

Semesterprojektet giver den studerende mulighed for i praksis, at arbejde med og vise forståelse for læringsmålene, i faget **Netværk- og kommunikationssikkerhed**.  
Den studerende skal kunne forstå og håndtere netværkssikkerhedstrusler samt implementere og
konfigurere udstyr til samme. 
Der bør i projektet lægges stor vægt på netværkssegmentering, monitorering samt implementering af relevante foranstaltninger såsom firewalls og logning af netværkstrafik. 
Desuden skal der fokuseres på det holistiske samspil med systemer (hosts, applikationer osv.).     
Læs læringsmålene for faget grundigt, da det er disse, der skal understøttes i projektet.

## Systemsikkerhed

Semesterprojektet giver den studerende mulighed for i praksis, at arbejde med og vise forståelse for læringsmålene, i faget **Systemsikkerhed**.  
Der skal opstilles to eller flere hosts på relevante netværk.  
Der bør lægges stor vægt på sikkerhedsovervågning samt implementering af foranstaltninger på operativsystemerne i projektet. 
Desuden skal der fokuseres på det holistiske samspil med netværket.  
Læs læringsmålene for faget grundigt, da det er disse, der skal understøttes i projektet.

## Generelle sikkerhedsprincipper og overlap mellem fagene.

Generelle sikkerhedsprincipper bør integreres i projektet som en holistisk tilgang til sikkerhed. Eksempelvis bør samspillet mellem begge fag støtte op omkring "Defense in Depth". Ligeledes kan princippet om netværkssegmentering og forskellige hosts' placering i segmenteringen understøtte princippet om "compartmentalization". Begge eksempler kan  sikre en helhedsorienteret tilgang, hvor forskellige sikkerhedsprincipper arbejder sammen for at styrke sikkerheden i systemet.  
Der lægges desuden vægt på gruppens evne til at samarbejde og kommunikere da dette er centralt for arbejdet med it-sikkerhed.

# Eksamination

Fagene **Netværks- og kommunikationssikkerhed** samt **systemsikkerhed** udprøves seperat som mundtlige individuelle eksamener, baseret på semesterprojektet. Det vil sige der er en eksamen per fag.  
Den afleverede rapport, dokumentation af projektet (ie. gitlab) og video demonstration danner grundlag for den mundtlige eksamen.  

Du kan finde detaljer om eksamen i [den institutionelle studieordning](https://esdhweb.ucl.dk/D24-2490519.pdf).

## Krav til skriftlig aflevering

Den skriftlige aflevering er en gruppe aflevering. Afleveringen er **en** samlet rapport der dækker begge fag.
Den skriftlige aflevering skal opfylde kravene til skriftlige opgaver, som er fastlagt i afsnit 3.1.5 i den [institutionelle studieordning](https://esdhweb.ucl.dk/D24-2490519.pdf).  
Yderligere skal afleveringen følge kravene, der er specificeret for prøverne i henholdsvis **Netværks- og kommunikationssikkerhed** samt **systemsikkerhed**, som er beskrevet i afsnit 3 af [institutionelle studieordning](https://esdhweb.ucl.dk/D24-2490519.pdf).

Vi anbefaler at i tager udgangspunkt i templaten [Formalia for de merkantile uddannelser i UCL på Seebladsgade](https://esdhweb.ucl.dk/D22-2039642.pdf) som også skal bruges i bachelor projektet.  

Vær opmærksom på, at det maksimale sideskrav pr. fag skal summeres. Hvis det højeste antal tilladte sider for Netværk og Kommunikationssikkerhed er 15, og det maksimale antal sider for Systemsikkerhed er også 15, må rapporten således højest være 30 sider.

Hver sektion i rapporten bør klart angive, om den omhandler et generelt emne eller er specifik for faget **Netværks- og kommunikationssikkerhed** eller **systemsikkerhed**. Som eksempel kan rapporten indeholde et afsnit kaldet "Firewall" med en generel beskrivelse af firewalls. Kapitlet kan herefter have underafsnit, der beskriver henholdsvis host-baserede firewalls (systemsikkerhed) og netværksbaserede firewalls (netværks- og kommunikationssikkerhed).  
Et andet eksempel kan være et kapitel om "Detektering", efterfulgt af underafsnit, der beskriver Network Intrusion Detection Systems (netværks- og kommunikationssikkerhed) og Host-based Intrusion Detection Systems (systemsikkerhed).

Udover de kapitler der er beskrevet i studieordningen, skal rapporten indeholde et kapitel der hedder _Proces_, som beskriver gruppens arbejdsproces. Endvidere skal gruppen vedlægge link til gruppens projektstyring (ie. gitlab), samt link til en video der demonstrerer gruppens praktiske implementation i et virtuelt miljø.  
Vi anbefaler at grupperne tager udgangsop´punkt i [Øvelse 1 - Gruppelog over aktiviteter](../exercises/01_gruppelog.md) som en guide til at skrive om hhv. proces og projektstyring.  

Den skriftlige rapport skal afleveres på Wiseflow den 31. maj.  
**OBS! Rapporten skal afleveres på Wiseflow i to eksamensflows, både i flowet til Netværk- og Kommunikationssikkerhed og i flowet til Systemsikkerhed.**

## Mundtlig præsentation

I den mundtlige præsentation til eksamenerne skal der lægges vægt på de emner og områder som understøtter læringsmålene fra det enkelte fag. Man bør dog indledningsvist give en introduktion til hele projektet.
Konkrete eksempler fra semesterprojektet vil blive vægtet højt. Det anbefales derfor at forberede sig på at præsentere og diskutere specifikke elementer og løsninger fra projektet til eksamen.

# Vejledning

I ugerne 19, 20 samt 21 er undervisningen i fagene **Netværks- og kommunikationssikkerhed** samt **systemsikkerhed** reserveret til arbejde med projektet. I løbet af disse uger vil underviserne være tilgængelige for vejledning. Se timeedit for vejledningsdatoer.  

Underviserne vil også vejlede i rapportskrivning, derfor skal grupperne sende en kopi af, eller link til, rapportkladden senest torsdag i hver af ugerne 19, 20 og 21.  

Det er gruppernes eget ansvar at opsøge og planlægge yderligere vejledning, så det passer ind i projektarbejdet.
