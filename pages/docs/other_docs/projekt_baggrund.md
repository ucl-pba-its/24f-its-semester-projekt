---
hide:
  - footer
---

# Information

Dette dokument beskriver de pædagogiske og didaktiske overvejelser der er anvendt som grundlag for projektet.

# Hvad skal de studerende lære?

Semesterprojektet giver den studerende mulighed for i praksis, at arbejde med og vise forståelse for læringsmålene, i faget **Netværk- og kommunikationssikkerhed** og **Systemsikkerhed**.  
En del af fagenes læringsmål kan kun opnås ved praktisk arbejde med sikring af it-miljøer.  
Dette praktiske arbejde har tidligere været udført i begrænset omfang gennem selvstændige øvelser i de respektice fag.  
Med udgangspunkt i fagenes læringsmål skal de studerende lære at implementere sikkerhed i et større virtuelt miljø og på den måde opnå en mere realistisk forståelse af hvordan sikkerhed implementeres i praksis. Dette opnås ved at de studerende arbejder sammen om at opbygge et virtuelt miljø der kan indeholde netværks- og systemkomponenter der kan bruges til opnå viden, færdigheder og kompetencer. 

- samarbejde - sikkerhed handler i høj grad om at kunne samarbejde (soft skills/21. century skills)

# Hvorfor skal de lære det?

- det står i læringsmålene (studieordningens afsnit)
- feedback fra tidligere studerende (semesterevalueringer)
- tæt på praksis (de studerende får en tættere tilknytning til praksis ved at arbejde i et realistisk miljø. I fremtiden plads til at virksomhedscase)

# Hvordan skal de lære det?

En voksen didaktisk tilgang, problembaseret og stor medindflydelse fra de studerende.
De praktiske opgaver gennem semestret danner grundlag for at arnbejde intensivt med projektet i projektperioden.

- Praktiske opgaver sætter alle 3 dimensioner i Illeris læringstrekant i spil.
- motivation - det at arbejde praktisk motiverer vores studerende (indre/ydre motivation)
- kontinuerlighed (at arbejde i et kontinuerligt miljø giver et mere holistisk syn på samspillet mellem sikkerhedskomponenter)
