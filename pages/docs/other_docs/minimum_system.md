---
hide:
  - footer
---

# Minimum system

Som minimum bør grupperne bygge et system der indeholder nedenstående komponenter og segmentering.

## System eksempel

![Image light theme](../images/system_diagram_0.1_light.png#only-light){align=center }  
![Image dark theme](../images/system_diagram_0.1_dark.png#only-dark){align=center }  


