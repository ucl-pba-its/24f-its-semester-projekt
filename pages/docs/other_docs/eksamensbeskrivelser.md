---
hide:
  - footer
---

# Dokumentets indhold

Dette dokument indeholder praktiske informationer samt spørgsmål til eksamen i faget: _xxx_.

# Eksamens beskrivelse

Eksamen er beskrevet i den institutionelle del af studieordningen _afsnit x.x.x_

Tidsplan for eksamen kan findes på wiseflow.  
Eksamen er med intern/ekstern censur.

*Indsæt beskrivelse af eksamenform her*

# Eksamens datoer

- Forsøg 1 - YYYY-MM-DD
- Forsøg 2 - YYYY-MM-DD
- Forsøg 3 - YYYY-MM-DD
