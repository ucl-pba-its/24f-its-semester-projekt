---
hide:
  - footer
---

# Introduktion

I uddannelsens studieordning beskrives kravene til undervisningen på uddannelsen. Dette betyder, at der i uddannelsens studieordning findes overordnede beskrivelser af de enkelte fag på uddannelsen samt beskrivelser af prøver og bedømmelsesgrundlag.

Studieordningerne er opdelt i to separate dokumenter: Den nationale studieordning, der opstiller kravene til uddannelsen på tværs af alle professionshøjskoler i landet, og den institutionelle studieordning, der kun gælder lokalt for den enkelte professionshøjskole.

## Institutionelle studieordning
Den institutionelle studieordning gør sig udelukkende gældende hos UCL og kan findes [her](https://esdhweb.ucl.dk/D24-2490519.pdf). Alle prøver på uddannelsen hos UCL er beskrevet i denne.

## Nationale studieordning
Den nationale studieordning gør sig gældende hos alle, der udbyder uddannelsen Pba. IT-sikkerhed, og kan findes [her](https://esdhweb.ucl.dk/D22-1980440.pdf).

## Kvalifikationsrammen for uddannelsen
Uddannelsens kvalifikationsramme er fastsat af Uddannelses- og Forskningsministeriet på niveau 6. Beskrivelsen af niveau 6 kan findes [her](https://ufm.dk/uddannelse/anerkendelse-og-dokumentation/dokumentation/kvalifikationsrammer/niveauer-i-kvalifikationsrammen/niveau-6).