---
hide:
  - footer
  - toc
---

# UCL PBa IT Sikkerhed - Semester projekt forår 2024

På denne side kan du finde informationer om semesterprojektet på it-sikkerhed i foråret 2024.

[Projektbeskrivelse](./other_docs/projektbeskrivelse.md) indeholder beskrivelse af og krav til projektet.   

[Dokumenter](./other_docs/studieordninger.md) indeholder materiale til brug i projektet.

[Øvelser](./exercises/01_gruppelog.md) indeholder øvelser til brug i projektet.