---
hide:
  - footer
---

# Øvelse 1 - Gruppelog over aktiviteter

## Information

Gruppelog er en fast øvelse der skal udføres af studiegruppen minimum en gang om ugen.  
Gruppens log skal afleveres sammen med eksamensrapporten i hhv.  
**Netværks- og kommunikationssikkerhed** og **Systemsikkerhed**.  
Loggen er en hjælp til at få indhold i rapportens obligatoriske procesafsnit, samt en mulighed for at følge med i egen læring. 

Gruppeloggen taler ind til læringsmål fra studieordningen hvor der står at, den uddannede kan:

- Håndtere komplekse situationer indenfor professionen
- Selvstændigt tilegne sig viden, færdigheder og kompetencer indenfor it-sikkerhed
- Påtage sig ansvar indenfor professionen samt indgå professionelt i tværfagligt samarbejde

## Instruktioner

Hver uge, senest torsdag eftermiddag, skal studiegruppen dokumentere deres fælles aktiviteter på gitlab pages.   
Screenshots, link til kode etc. I dokumentationen.   
Undervisere læser dokumentation fredag og giver feedback efter behov. 
  
Fast punkter i loggen skal som minimum være: 

  - Hvordan har vi arbejdet med ugens læringsmål.    
  - Hvilke af vores aktiviteter passer på læringsmålene og hvad synes vi at vi har lært?   
  - Hvad synes vi at vi ikke nåede at lære? 
  - Guide til det vi har lavet praktisk, skrevet så andre (og os selv) kan reproducere det tekniske vi har lavet.   
  
## Links

Læringsmål for uddannelsen: [https://esdhweb.ucl.dk/D22-1980440.pdf](https://esdhweb.ucl.dk/D22-1980440.pdf) 